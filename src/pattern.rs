// SPDX-License-Identifier: GPL-3.0-or-later
// (C) Copyright 2025 Kunal Mehta <legoktm@debian.org>
/// Wiki-specific patterns to cleanup; they operate on wikitext instead of HTML
use crate::Summary;
use regex::Regex;
use std::borrow::Cow;

pub(crate) fn replace_welcome<'a>(
    input: &'a str,
    summary: &mut Summary,
) -> Cow<'a, str> {
    let regex = Regex::new(r#"([\|!] <div style="margin.*Hello,.*Welcome\]\] to Wikipedia.* Please remember to .* Happy editing! .* \d\d\d\d \(UTC\))(\n\|})"#).unwrap();
    if regex.is_match(input) {
        summary.missing_end_tag += 1;
        summary
            .tags
            .insert("Template:Welcomeg/WelcomeMenu".to_string());
        regex.replace_all(input, "$1</div>$2")
    } else {
        Cow::from(input)
    }
}
